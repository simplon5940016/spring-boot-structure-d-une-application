# Java - Spring Boot - Brief 1

## Qu'est-ce que Spring ?

Spring est un framework de Java qui a vu sa première version sortir en 2003. L'objectif de Spring est de simplifier le développement d'applications Java. Spring est construit autour du concept dd'inversion de contrôle et d'injection de dépendances. 
Le principal atout de Spring est la maintenabilité du code dans le temps. 

## Quelle est la différence entre Spring et Spring Boot ?

La principale différence entre les deux est que Spring nécessite d'être configuré alors que Spring boot non. 
Spring boot est basé sur toutes les fonctionnalités de Spring, il permet de réduire la difficulté de configuration de Spring. 
Le seul désavantage pourrait être le manque de flexibilité lors de la configuration, cependant cela n'est utile que dans des cas très particuliers.

## Qu'est-ce que l'inversion de contrôle ?

L'inversion de contrôle permet de réduire la dépendance entre le code et une classe autre. Les objets sont ainsi créés et gérés par un conteneur IoC, qui est responsable de la création, de la configuration et de la gestion des objets. Le conteneur IoC utilise des métadonnées (comme des fichiers de configuration) pour déterminer comment créer et configurer les objets.

## Qu'est-ce que l'injection de dépendance ?

L'injection de dépendance est souvent en lien avec l'IoC. L'injection de dépendance est une technique de programmation qui permet de fournir à un objet les autres objets dont il a besoin pour fonctionner. En d'autres termes, une fois que les objets sont configurés via l'IoC, ceux-ci sont ensuite injectés dans les autres objets qui en ont besoin, plutôt que d'être créés (via new ()) directement par ces objets. Cela permet de réduire le couplage entre les objets et de rendre le code plus modulaire. Les objets peuvent être facilement remplacés ou modifiés sans affecter le reste de l'application. 


